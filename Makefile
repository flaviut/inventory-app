# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2018 Sébastien Eustace

POETRY_RELEASE := $$(sed -n -E "s/__version__ = '(.+)'/\1/p" poetry/__version__.py)

# lists all available targets
list:
	@sh -c "$(MAKE) -p no_targets__ | \
		awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {\
			split(\$$1,A,/ /);for(i in A)print A[i]\
		}' | grep -v '__\$$' | grep -v 'make\[1\]' | grep -v 'Makefile' | sort"
# required for list
no_targets__:

install-deps:
	poetry install
	cd frontend && yarn install

format: install-deps
	poetry run black inventory tests
	poetry run isort -rc inventory tests
	cd frontend && yarn run prettier --write '{src,public}/**/*.{js,jsx,ts,tsx,css,html,svg,json}' '*.json'

lint: format install-deps
	poetry run prospector inventory
	poetry run prospector tests

format-check: install-deps
	poetry run black --check inventory tests
	poetry run isort -rc --check-only inventory tests
	cd frontend && yarn run prettier --check '{src,public}/**/*.{js,jsx,ts,tsx,css,html,svg,json}' '*.json'

lint-check: format-check install-deps
	poetry run prospector inventory
	poetry run prospector tests

test: install-deps
	poetry run py.test -vv tests/
	poetry run coverage html
	poetry run coverage report -m

tox:
	@tox
