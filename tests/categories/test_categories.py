import json

from django.test import Client

import funcy

from inventory.util import random_string
from tests.parameter.test_enum import create_enum_parameter
from tests.parameter.test_scalar import create_scalar_parameter

_URL = "/api/category"


def create_category(admin_client: Client):
    enum_param = create_enum_parameter(admin_client)
    scalar_param = create_scalar_parameter(admin_client)

    content_in = {
        "name": random_string(),
        "description": "asdf",
        "parent": None,
        "parameters": [enum_param["url"], scalar_param["url"]],
    }

    response = admin_client.post(
        f"{_URL}/", content_type="application/json", data=json.dumps(content_in)
    )
    content = json.loads(response.content)
    content_test = funcy.omit(
        {
            **content_in,
            # depth-first parameter ordering is required
            "all_parameters": content_in["parameters"],
        },
        ["url"],
    )
    assert content_test == funcy.omit(content, ["url"])
    assert response.status_code == 201

    return content


def test_create_category(admin_client: Client):
    create_category(admin_client)


def test_update_category(admin_client: Client):
    category_1 = create_category(admin_client)
    category_2 = create_category(admin_client)

    content_in = {
        **category_2,
        "name": random_string(),
        "description": "gasdsf",
        "parent": category_1["url"],
        "parameters": [category_2["parameters"][0]],
    }
    response = admin_client.patch(
        category_2["url"], content_type="application/json", data=json.dumps(content_in)
    )
    content = json.loads(response.content)
    content_test = {
        **content_in,
        "all_parameters": [*category_1["parameters"], *content_in["parameters"]],
    }
    assert content_test == content
    assert response.status_code == 200


def test_category_inherits_parent_parameters(admin_client: Client):
    category_1 = create_category(admin_client)
    category_2 = create_category(admin_client)

    def nested_1_deep():
        content_in_2 = {**category_2, "parent": category_1["url"]}
        response_2 = admin_client.patch(
            content_in_2["url"],
            content_type="application/json",
            data=json.dumps(content_in_2),
        )
        content_2 = json.loads(response_2.content)
        content_test_2 = {
            **content_in_2,
            # depth-first parameter ordering is required
            "all_parameters": [*category_1["parameters"], *category_2["parameters"]],
        }
        assert content_test_2 == content_2
        assert response_2.status_code == 200

    nested_1_deep()

    def nested_2_deep():
        category_3 = create_category(admin_client)
        content_in_3 = {**category_3, "parent": category_2["url"]}
        response_3 = admin_client.patch(
            content_in_3["url"],
            content_type="application/json",
            data=json.dumps(content_in_3),
        )
        content_3 = json.loads(response_3.content)
        content_test_3 = {
            **content_in_3,
            # depth-first parameter ordering is required
            "all_parameters": [
                *category_1["parameters"],
                *category_2["parameters"],
                *category_3["parameters"],
            ],
        }
        assert content_test_3 == content_3
        assert response_3.status_code == 200

    nested_2_deep()
