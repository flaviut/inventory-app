import pytest


# create the test db so that Hypothesis doesn't complain
# noinspection PyUnusedLocal
@pytest.mark.django_db
def test_init():
    # import everything so that Hypothesis doesn't complain about the first
    # test being too slow
    from inventory import models, views, api

    models_ = models  # noqa: F841
    views_ = views  # noqa: F841
    api_ = api  # noqa: F841
