import json
from typing import Any, Dict

from django.test import Client

import funcy as fc

from inventory.util import random_string

_URL = "/api/parameter-type"


def remove_ids(content: Dict[str, Any]) -> Dict[str, Any]:
    return {
        **fc.omit(content, ["url"]),
        "value_set": (
            [fc.omit(value, ["id"]) for value in content["value_set"]]
            if (content.get("value_set") is not None)
            else None
        ),
    }


def create_enum_parameter(admin_client: Client) -> Dict[str, Any]:
    content_in = {
        "name": random_string(),
        "description": "bar",
        "kind": "enum",
        "value_set": [
            {"name": "a", "description": "123"},
            {"name": "b", "description": "444"},
        ],
    }
    response_create = admin_client.post(
        f"{_URL}/", content_type="application/json", data=json.dumps(content_in)
    )
    assert response_create.status_code == 201
    content = json.loads(response_create.content)

    content_test = content_in
    assert content_test == remove_ids(content)

    return content


def test_create_invalid_enum(admin_client: Client):
    content_in = {"name": "fooe", "description": "bar", "kind": "enum", "value_set": []}
    response_create = admin_client.post(
        f"{_URL}/", content_type="application/json", data=json.dumps(content_in)
    )
    assert response_create.status_code == 400

    content_in_2 = {**content_in, "value_set": None}
    response_create = admin_client.post(
        f"{_URL}/", content_type="application/json", data=json.dumps(content_in_2)
    )
    assert response_create.status_code == 400

    for field, value in [("unit", "count"), ("min_value", 0), ("max_value", 10)]:
        response_create = admin_client.post(
            f"{_URL}/",
            content_type="application/json",
            data=json.dumps({**content_in, field: value}),
        )
        assert response_create.status_code == 400


def test_create_enum_parameter(admin_client: Client):
    create_enum_parameter(admin_client)


def update_enum_test_inner(admin_client: Client, modifier):
    parameter = create_enum_parameter(admin_client)

    # add a new enum value
    changed_parameter = modifier(parameter)
    response = admin_client.patch(
        changed_parameter["url"],
        content_type="application/json",
        data=json.dumps(changed_parameter),
    )
    assert response.status_code == 200
    content = json.loads(response.content)
    assert remove_ids(changed_parameter) == remove_ids(content)


# add a new enum value
def test_update_enum_new_value(admin_client: Client):
    def add_new_value(parameter):
        return {
            **parameter,
            "value_set": parameter["value_set"] + [{"name": "c", "description": "43"}],
        }

    update_enum_test_inner(admin_client, add_new_value)


def test_update_enum_remove_value(admin_client: Client):
    # add a new enum value
    def remove_value(parameter):
        return {**parameter, "value_set": parameter["value_set"][0:-1]}

    update_enum_test_inner(admin_client, remove_value)


def test_update_enum_rename_value(admin_client: Client):
    def change_value_name(parameter):
        return {
            **parameter,
            "value_set": [
                parameter["value_set"][0],
                {**parameter["value_set"][1], "name": "afgs"},
            ],
        }

        update_enum_test_inner(admin_client, change_value_name)
