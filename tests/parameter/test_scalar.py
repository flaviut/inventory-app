import json
from typing import Any, Dict

from django.test import Client

import funcy
import pytest
from hypothesis import given, strategies

from inventory.models import ParameterType
from inventory.util import random_string

_URL = "/api/parameter-type"

REASONABLE_FLOATS = dict(
    min_value=-1e15, max_value=1e15, allow_nan=False, allow_infinity=False
)


@given(
    minv=strategies.floats(**REASONABLE_FLOATS),
    maxv=strategies.floats(**REASONABLE_FLOATS),
    desc=strategies.text(
        # blacklist null and unmatched surrogates
        strategies.characters(blacklist_categories=("Cc", "Cs"))
    ),
    name=strategies.text(
        strategies.characters(blacklist_categories=("Cc", "Cs")),
        min_size=1,
        max_size=ParameterType._meta.get_field("name").max_length,
        # skip all-whitespace text
    ).filter(lambda name: name.strip()),
)
def test_create_scalar_parameter(
    subtest, minv: float, maxv: float, name: str, desc: str
):
    @subtest
    def inner(admin_client: Client):
        content_in = {
            "name": name,
            "description": desc,
            "kind": "scalar",
            "unit": "count",
            "min_value": minv,
            "max_value": maxv,
        }
        content_test = funcy.omit(
            {
                **content_in,
                "min_value": pytest.approx(minv, rel=1e-12),
                "max_value": pytest.approx(maxv, rel=1e-12),
                "name": name.strip(),
                "description": desc.strip(),
            },
            ["url"],
        )
        response = admin_client.post(
            f"{_URL}/", content_type="application/json", data=json.dumps(content_in)
        )
        content = json.loads(response.content)
        assert content_test == funcy.omit(content, ["url"])
        assert response.status_code == 201


def create_scalar_parameter(admin_client: Client) -> Dict[str, Any]:
    content_in = {
        "name": random_string(),
        "description": "bar",
        "kind": "scalar",
        "unit": "count",
        "min_value": -10,
        "max_value": 10,
    }
    response_create = admin_client.post(
        f"{_URL}/", content_type="application/json", data=json.dumps(content_in)
    )
    assert response_create.status_code == 201
    content = json.loads(response_create.content)

    content_test = funcy.omit(content_in, ["url"])
    assert content_test == funcy.omit(content, ["url"])

    return content


def test_get_scalar_parameter(admin_client: Client):
    created_content = create_scalar_parameter(admin_client)

    response = admin_client.get(created_content["url"])
    content = json.loads(response.content)
    assert content == created_content


def test_create_invalid_scalar(admin_client: Client):
    content_in = {
        "name": "foo",
        "description": "bar",
        "kind": "scalar",
        "unit": "count",
        "min_value": -10,
        "max_value": 10,
    }
    for field in ["unit", "min_value", "max_value"]:
        response_create = admin_client.post(
            f"{_URL}/",
            content_type="application/json",
            data=json.dumps(funcy.omit(content_in, [field])),
        )
        assert response_create.status_code == 400


def test_update_scalar_parameter(admin_client: Client):
    created_content = create_scalar_parameter(admin_client)

    new_values = {
        "name": "abc",
        "description": "123",
        "kind": "scalar",
        "unit": "meter",
        "min_value": -20,
        "max_value": 20,
    }

    response = admin_client.patch(
        created_content["url"],
        content_type="application/json",
        data=json.dumps(new_values),
    )
    assert response.status_code == 200
    content = json.loads(response.content)
    content_test = funcy.omit(content, ["url"])

    assert new_values == funcy.omit(content_test, ["url"])
