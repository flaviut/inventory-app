import json

from django.test import Client

from tests.parameter.test_enum import create_enum_parameter
from tests.parameter.test_scalar import create_scalar_parameter


def test_convert_enum_to_scalar_fails(admin_client: Client):
    parameter_enum = create_enum_parameter(admin_client)
    response = admin_client.patch(
        parameter_enum["url"],
        content_type="application/json",
        data=json.dumps(
            {"kind": "scalar", "min_value": 0, "max_value": 10, "unit": "count"}
        ),
    )
    # must fail
    assert response.status_code == 400
    content = json.loads(response.content)
    assert "kind" in content  # this is an error on kind


def test_convert_scalar_to_enum_fails(admin_client: Client):
    parameter_scalar = create_scalar_parameter(admin_client)
    response = admin_client.patch(
        parameter_scalar["url"],
        content_type="application/json",
        data=json.dumps(
            {"kind": "enum", "value_set": [{"name": "foo", "description": ""}]}
        ),
    )
    # must fail
    assert response.status_code == 400
    content = json.loads(response.content)
    assert "kind" in content  # this is an error on kind


def test_delete_parameter(admin_client: Client):
    created_scalar = create_scalar_parameter(admin_client)
    response_scalar = admin_client.delete(created_scalar["url"])
    assert response_scalar.status_code == 204
    get_scalar = admin_client.get(
        created_scalar["url"], content_type="application/json"
    )
    assert get_scalar.status_code == 404

    created_enum = create_enum_parameter(admin_client)
    response_enum = admin_client.delete(created_enum["url"])
    assert response_enum.status_code == 204
    get_enum = admin_client.get(created_enum["url"], content_type="application/json")
    assert get_enum.status_code == 404
