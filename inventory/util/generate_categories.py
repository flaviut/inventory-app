from typing import List, Optional, Union

from inventory.models import Category

ELECTRONICS_CATEGORIES = [
    [
        "Passives",
        [
            ["Capacitors", ["Eletrolytic", "Tantalum", "Ceramic", "Adjustable"]],
            ["Resistors", ["Adjustable"]],
            ["Inductors", ["Ferrite beads"]],
            "Fuses",
        ],
    ],
    ["Diodes", ["LEDs", "ESD protection"]],
    [
        "ICs",
        [
            "Opamps",
            "FPGAs",
            ["Power", ["Switching regulators", "Linear regulators", "MOSFETs"]],
        ],
    ],
    "Crystals",
    "Sensors",
    "Cables",
    ["Connectors", ["Audio", "D-Sub", "USB", "Barrel"]],
    ["Switches", ["Relays", "Pushbutton"]],
]

CategoryTree = List[Union[List["CategoryTree"], "CategoryTree"]]


def generate_category_models(
    categories: CategoryTree, parent: Optional[Category] = None
) -> List[Category]:
    models: List[Category] = [parent]
    for elem in categories:
        if isinstance(elem, List):
            models.append(Category(name=elem[0], parent=parent))
            models[-1].save()
            models.extend(generate_category_models(elem[1], parent=models[-1]))
        elif isinstance(elem, str):
            models.append(Category(name=elem, parent=parent))
            models[-1].save()

    return models[1:]
