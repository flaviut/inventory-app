from typing import Optional, Union

from django.db import models

from inventory.util.units import UNIT_REGISTRY


class UnitField(models.CharField):
    """A field that represents a specific unit."""

    def __init__(self, *args, max_length=120, **kwargs):
        # pylint: disable=useless-super-delegation
        super().__init__(*args, max_length=max_length, **kwargs)
        self.validators.clear()  # don't validate length

    def to_python(
        self, value: Optional[Union[str, UNIT_REGISTRY.Unit]]
    ) -> UNIT_REGISTRY.Unit:
        if value is None:
            return None
        if isinstance(value, UNIT_REGISTRY.Unit):
            return value

        return UNIT_REGISTRY.get_base_units(value)[1]

    def get_prep_value(self, value) -> Optional[str]:
        if value is None:
            return None
        return str(value)
