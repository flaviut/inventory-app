import random
from typing import List

from django.db.utils import IntegrityError

from faker import Faker

from inventory.util.generate_categories import (
    ELECTRONICS_CATEGORIES,
    generate_category_models,
)
from inventory.util.units import UNIT_REGISTRY

from ..models import (
    Category,
    Distributor,
    DistributorPart,
    EnumValue,
    Order,
    OrderPart,
    ParameterType,
    ParameterValue,
    Part,
)

FAKE = Faker()


def generate_category() -> List[Category]:
    try:
        return generate_category_models(ELECTRONICS_CATEGORIES)
    except IntegrityError:
        pass


def generate_part(count: int):
    for _ in range(0, count):
        try:
            Part(
                part_number=FAKE.isbn10(separator="-"),
                category=Category.objects.order_by("?")[0],
            ).save()
        except IntegrityError:
            pass


def generate_enum_parameter(count: int):
    for _ in range(0, count):
        try:
            enum_kind = ParameterType(name=FAKE.last_name())
            enum_kind.save()
            for _ in range(0, 5):
                EnumValue(name=FAKE.last_name(), kind=enum_kind).save()
        except IntegrityError as exc:
            pass


def generate_numeric_parameters(count: int):
    for _ in range(0, count):
        try:
            ParameterType(
                name=FAKE.last_name(),
                unit=random.choice(list(UNIT_REGISTRY._units.keys())),
                min_value=10 ** random.uniform(-6, 0),
                max_value=10 ** random.uniform(0, 6),
                category=Category.objects.order_by("?")[0],
            ).save(),
        except IntegrityError:
            pass


def generate_parameter_value(count: int):
    for _ in range(0, count):
        try:
            parameter_type = (
                ParameterType.objects.filter(unit__isnull=False).order_by("?")
            )[0]
            ParameterValue(
                parameter_type=parameter_type,
                part=Part.objects.order_by("?")[0],
                num_value=random.uniform(
                    float(parameter_type.min_value), float(parameter_type.max_value)
                ),
            ).save()
        except IntegrityError:
            pass


def generate_distributor() -> Distributor:
    return Distributor()


def generate_distributor_part() -> DistributorPart:
    return DistributorPart()


def generate_order() -> Order:
    return Order()


def generate_order_part() -> OrderPart:
    return OrderPart()
