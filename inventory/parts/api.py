from django.db.models import Sum
from django.db.models.functions import Coalesce

from rest_framework import serializers, viewsets
from rest_framework.routers import BaseRouter

from inventory.api_common import DefaultPagination
from inventory.models import ParameterValue, Part


class ParameterValueSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ParameterValue
        fields = ("parameter_type", "num_value", "enum_value")


class PartSerializer(serializers.HyperlinkedModelSerializer):
    parameters = ParameterValueSerializer(many=True, read_only=True)
    in_stock = serializers.CharField(read_only=True)

    class Meta:
        model = Part
        fields = (
            "url",
            "part_number",
            "description",
            "category",
            "stock_unit",
            "in_stock",
            "parameters",
            "photos",
        )


class PartViewSet(viewsets.ModelViewSet):
    queryset = (
        Part.objects.annotate(in_stock=Coalesce(Sum("orderpart__quantity"), 0))
        .prefetch_related("parameters")
        .all()
    )
    serializer_class = PartSerializer
    pagination_class = DefaultPagination
    filter_fields = ("category",)


def register(router: BaseRouter):
    router.register("part", PartViewSet)
