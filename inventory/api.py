from django.contrib.auth.models import User

from rest_framework import routers, serializers, viewsets

from inventory.api_common import DefaultPagination
from inventory.categories import api as category_api
from inventory.parameters import api as parameter_api
from inventory.parts import api as part_api


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ("url", "username", "email", "is_staff")


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = DefaultPagination


# Routers provide an easy way of automatically determining the URL conf.
# pylint: disable=invalid-name
router = routers.DefaultRouter()
router.register("user", UserViewSet)
part_api.register(router)
category_api.register(router)
parameter_api.register(router)
