from django.contrib import admin

from mptt.admin import MPTTModelAdmin

from inventory.models import (
    Category,
    Distributor,
    DistributorPart,
    EnumValue,
    Order,
    OrderPart,
    ParameterType,
    ParameterValue,
    Part,
)

admin.site.register(Category, MPTTModelAdmin)
admin.site.register(Part)
admin.site.register(EnumValue)
admin.site.register(ParameterType)
admin.site.register(ParameterValue)
admin.site.register(Distributor)
admin.site.register(DistributorPart)
admin.site.register(Order)
admin.site.register(OrderPart)
