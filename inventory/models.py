from typing import List

from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Model, Q

from babel.numbers import get_currency_name, list_currencies
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from inventory.util.fields import UnitField

CURRENCY_CHOICES = sorted(
    [(currency_id, get_currency_name(currency_id)) for currency_id in list_currencies()]
)


class ParameterType(Model):
    name = models.CharField(max_length=80, unique=True)
    description = models.TextField(default="", blank=True)

    unit = UnitField(null=True, default=None)
    min_value = models.FloatField(null=True, default=None)
    max_value = models.FloatField(null=True, default=None)

    def __str__(self):
        return self.name

    class Meta:
        constraints = [
            models.CheckConstraint(
                name="either_enum_or_num",
                check=(
                    Q(
                        unit__isnull=False,
                        min_value__isnull=False,
                        max_value__isnull=False,
                    )
                    | Q(
                        unit__isnull=True,
                        min_value__isnull=True,
                        max_value__isnull=True,
                    )
                ),
            )
        ]


class EnumValue(Model):
    name = models.CharField(max_length=80)
    description = models.TextField(default="", blank=True)
    kind = models.ForeignKey(
        ParameterType, on_delete=models.CASCADE, related_name="value_set"
    )

    def __str__(self):
        return f"{self.kind.name}.{self.name}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="unique_enum_and_kind", fields=("name", "kind")
            )
        ]


class Category(MPTTModel):
    name = models.CharField(max_length=50)
    description = models.TextField(default="", blank=True)

    parent = TreeForeignKey(
        "self",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        related_name="subcategories",
    )

    parameters = models.ManyToManyField(ParameterType, blank=True)

    @property
    def all_parameters(self) -> List[ParameterType]:
        parameters = []
        for node in self.get_ancestors(include_self=True):
            parameters = [*parameters, *node.parameters.all()]
        return parameters

    def __str__(self):
        return self.name

    class MPTTMeta:
        order_insertion_by = ["name"]

    class Meta:
        verbose_name_plural = "categories"
        constraints = [
            models.UniqueConstraint(
                name="unique_even_parent_null",
                fields=["name"],
                condition=Q(parent__isnull=True),
            ),
            models.UniqueConstraint(
                name="unique_when_not_null",
                fields=["name", "parent"],
                condition=Q(parent__isnull=False),
            ),
        ]


class Part(Model):
    part_number = models.CharField(max_length=200, unique=True)
    description = models.TextField(default="", blank=True)

    category = models.ForeignKey(Category, on_delete=models.PROTECT, null=False)

    minimum_stock_level = models.FloatField(
        default=0, validators=[MinValueValidator(0)]
    )
    stock_unit = UnitField(default="count")


class PartPhoto(Model):
    uri = models.CharField(max_length=200, null=False)
    part = models.ForeignKey(
        Part, on_delete=models.CASCADE, null=False, related_name="photos"
    )
    display_order = models.IntegerField(null=False, default=0)


class ParameterValue(Model):
    parameter_type = models.ForeignKey(ParameterType, on_delete=models.PROTECT)
    part = models.ForeignKey(Part, on_delete=models.CASCADE, related_name="parameters")

    num_value = models.FloatField(null=True, default=None)
    enum_value = models.ForeignKey(
        EnumValue,
        on_delete=models.PROTECT,
        null=True,
        default=None,
        related_name="parametervalue_set",
    )

    class Meta:
        constraints = [
            models.CheckConstraint(
                name="either_enum_or_num",
                # either we have an enum value or a numeric value, but not both
                check=(
                    Q(num_value__isnull=False, enum_value__isnull=True)
                    | Q(num_value__isnull=True, enum_value__isnull=False)
                ),
            )
        ]
        unique_together = [("parameter_type", "part")]


class Distributor(Model):
    name = models.CharField(max_length=80, unique=True)
    url = models.CharField(max_length=120, unique=True)

    def __str__(self):
        return self.name


class DistributorPart(Model):
    """Distributors usually have their own part numbers"""

    part_number = models.CharField(max_length=80, unique=True)
    distributor = models.ForeignKey(Distributor, on_delete=models.CASCADE)
    part = models.ForeignKey(Part, on_delete=models.CASCADE)

    class Meta:
        unique_together = [("part_number", "distributor")]


class Order(Model):
    distributor = models.ForeignKey(Distributor, on_delete=models.PROTECT)
    order_number = models.CharField(max_length=40)

    additional_cost = models.FloatField()
    currency = models.CharField(max_length=3, choices=CURRENCY_CHOICES)

    class Meta:
        unique_together = [("distributor", "order_number")]


class OrderPart(Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    part = models.ForeignKey(Part, on_delete=models.PROTECT)

    unit_price = models.FloatField()
    quantity = models.FloatField()
