from typing import Any, Dict, List

from django.db import models, transaction
from django.db.models import Case, Count, ProtectedError, Value, When

import funcy as fc
from rest_framework import serializers, viewsets
from rest_framework.fields import ChoiceField
from rest_framework.routers import BaseRouter

from inventory.api_common import DefaultPagination
from inventory.models import EnumValue, ParameterType


class EnumValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnumValue
        fields = ("id", "name", "description")


class ParameterTypeSerializer(serializers.HyperlinkedModelSerializer):
    value_set = EnumValueSerializer(many=True, required=False)
    kind = ChoiceField(choices=["enum", "scalar"], required=True)

    class Meta:
        model = ParameterType
        fields = (
            "url",
            "name",
            "description",
            "kind",
            "unit",
            "min_value",
            "max_value",
            "value_set",
        )

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        if ret["kind"] == "enum":
            for field in ["unit", "min_value", "max_value"]:
                # delete field if exists
                # https://stackoverflow.com/a/15411146/2299084
                ret.pop(field, None)
        else:
            ret.pop("value_set", None)
        return ret

    def to_internal_value(self, data):
        ret = fc.project(
            super().to_internal_value(data), ["url", "kind", "name", "description"]
        )

        kind = data["kind"]
        if kind == "enum":
            invalid_fields = ["unit", "min_value", "max_value"]
            # set the field to none if it doesn't exist, it'll get validated later
            ret["value_set"] = data.get("value_set", None)
        elif kind == "scalar":
            invalid_fields = ["value_set"]
            for valid_field in ["unit", "min_value", "max_value"]:
                # set the field to none if it doesn't exist, it'll get validated later
                ret[valid_field] = data.get(valid_field, None)
        else:
            raise AssertionError("kind validation is buggy")

        errors = {
            field: 'Field can\'t be set for kind "{}"'.format(kind)
            for field in invalid_fields
            if data.get(field, None) is not None
        }
        if errors:
            raise serializers.ValidationError(errors)

        return ret

    def validate(self, data):
        kind = data["kind"]
        if kind == "enum":
            if not data.get("value_set", None):
                raise serializers.ValidationError(
                    {
                        "value_set": "Enum parameter types must define at least one "
                        "possible value"
                    }
                )
        elif kind == "scalar":
            errors = {
                field: "Must be set for scalar parameter types"
                for field in ["unit", "min_value", "max_value"]
                if data.get(field, None) is None
            }
            if errors:
                raise serializers.ValidationError(errors)
        else:
            raise AssertionError("kind validation is buggy")

        return data

    @transaction.atomic
    def create(self, validated_data: Dict[str, Any]) -> ParameterType:
        if validated_data["kind"] == "scalar":
            del validated_data["kind"]
            res: ParameterType = super().create(validated_data)
            res.kind = "scalar"
            return res
        else:
            del validated_data["kind"]
            value_set = validated_data.pop("value_set")
            parameter = ParameterType.objects.create(**validated_data)
            for value in value_set:
                EnumValue.objects.create(**value, kind=parameter)
            parameter.kind = "enum"
            return parameter

    @transaction.atomic
    def update(
        self, instance: ParameterType, validated_data: Dict[str, Any]
    ) -> ParameterType:
        if validated_data["kind"] == "scalar":
            if instance.unit is None:
                raise serializers.ValidationError(
                    {"kind": "Can't convert scalar parameters to enums"}
                )

            del validated_data["kind"]
            res: ParameterType = super().update(instance, validated_data)
            res.kind = "scalar"
            return res
        else:
            if instance.unit is not None:
                raise serializers.ValidationError(
                    {"kind": "Can't convert enum parameters to scalars"}
                )

            del validated_data["kind"]
            value_set = validated_data.pop("value_set")

            for key in ["name", "description"]:
                setattr(instance, key, validated_data[key])
            instance.save()

            # existing values - new values = values to delete
            # fields a protected from deletion if they are referenced anywhere
            values_to_remove = set(
                instance.value_set.values_list("id", flat=True)
            ) - set(
                fc.rcompose(
                    fc.partial(fc.filter, lambda v: v.get("id", None) is not None),
                    fc.partial(fc.pluck, "id"),
                )(value_set)
            )
            try:
                (
                    EnumValue.objects.filter(
                        kind=instance, id__in=values_to_remove
                    ).delete()
                )
            except ProtectedError:
                used_enum_values: List[str] = (
                    EnumValue.objects.annotate(value_count=Count("parametervalue_set"))
                    .filter(kind=instance, name__in=values_to_remove, value_count__ne=0)
                    .values_list("name", flat=True)
                )
                formatted_used_values = ", ".join(used_enum_values)
                raise serializers.ValidationError(
                    {
                        "value_set": "Can't delete enum value(s) "
                        f"{formatted_used_values} as they are still in use"
                    }
                )

            for value in value_set:
                if value.get("id", None) is not None:
                    (
                        EnumValue.objects.filter(id=value["id"], kind=instance).update(
                            **value
                        )
                    )
                else:
                    EnumValue.objects.create(kind=instance, **value)

            instance.kind = "enum"
            return instance


class ParameterTypeViewSet(viewsets.ModelViewSet):
    queryset = (
        ParameterType.objects.all()
        .annotate(
            kind=Case(
                When(unit__isnull=False, then=Value("scalar")),
                When(unit__isnull=True, then=Value("enum")),
                output_field=models.CharField(),
            )
        )
        .prefetch_related("value_set")
    )
    serializer_class = ParameterTypeSerializer
    pagination_class = DefaultPagination
    filter_fields = ("id",)


def register(router: BaseRouter):
    router.register("parameter-type", ParameterTypeViewSet)
