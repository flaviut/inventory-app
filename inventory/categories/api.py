from rest_framework import serializers, viewsets
from rest_framework.relations import HyperlinkedRelatedField
from rest_framework.routers import BaseRouter

from inventory.models import Category


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    all_parameters = HyperlinkedRelatedField(
        view_name="parametertype-detail", many=True, read_only=True
    )

    class Meta:
        model = Category
        fields = (
            "url",
            "name",
            "description",
            "parent",
            "parameters",
            "all_parameters",
        )


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all().prefetch_related("parameters")


def register(router: BaseRouter):
    router.register("category", CategoryViewSet, basename="category")
