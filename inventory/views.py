from django.shortcuts import redirect

from inventory.util.generate_dummy import (
    generate_category,
    generate_enum_parameter,
    generate_numeric_parameters,
    generate_parameter_value,
    generate_part,
)


def gen_all(request):
    generate_category()
    generate_part(1000)
    generate_enum_parameter(20)
    generate_numeric_parameters(20)
    generate_parameter_value(10000)
    return redirect(to="/api/")
