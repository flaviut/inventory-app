# Electronics Catalog

This a tool for keeping track of your electronic parts. It is inspired by
various electronics catalogs, like 

- [DigiKey](https://www.digikey.com/products/en/resistors/chip-resistor-surface-mount/52)
- [Mouser](https://www.mouser.com/Passive-Components/Resistors/SMD-Resistors-Chip-Resistors/_/N-7h7yu)
- [LCSC](https://lcsc.com/products/Chip-Resistor-Surface-Mount_439.html)

## Backend

The backend is implemented in Python 3.7+, [Django][], and PostgreSQL

### Getting started

1. Follow [these instructions to install poetry][poetry-install], the tool we
use to install dependencies and build packages.
2. Run `poetry install` in order to install dependencies
3. Run `poetry shell` in order to open a shell in the virtual environment
4. Start the database server using Docker and the following command: 
   `sudo docker run --rm -v $PWD/db:/var/lib/postgresql/data postgres:11-alpine`
5. In the poetry shell, run `./manage.py migrate` in order to initialize the
   development database 
6. Run `make test` in order to verify that the tests work & everything is
   installed correctly. 

[poetry-install]: https://github.com/sdispater/poetry#installation
[django]: https://docs.djangoproject.com/en/2.2/

#### Development server

To start a development server, run `./manage.py runserver` inside the shell. You
will need to modify `DATABASES.default.HOST` in `inventory/settings.py` to
contain the IP address of the dockerized database. Do not commit this changed
IP.

You can find the database's IP using this command: `sudo docker inspect $(sudo
docker ps | awk '/postgres:11-alpine/ { print $1 }') | awk '/IPAddress.*\./ {
print $2; exit }'`

To log into the admin interface, run `./manage.py createsuperuser` in a poetry
shell. You will be prompted for a username & password. You can then use these
credentials to access [the admin console](http://127.0.0.1:8000/admin/).

#### Unit tests

To run the tests, run `make test`.

#### Verify code quality

Before committing, run `make lint` and fix any lint issues that arise. This will
also reformat the code to follow the style guide.
