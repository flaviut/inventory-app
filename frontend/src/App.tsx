import React, { Component } from "react";
import "./App.css";
import { PopulatedPartTable } from "./pages/PopulatedPartTable";
import { Provider } from "unstated";
import { PopulatedCategoryTree } from "./pages/PopulatedCategoryTree";
import { Row, Col } from "antd";
import { CategoriesContainer } from "./models/Categories";

const categoryStore = new CategoriesContainer();
categoryStore.fetchAll();

export default class App extends Component<
  {},
  { selectedCategory: string | null }
> {
  private populatedCategoryTree = (
    <PopulatedCategoryTree onCategorySelect={this.onSelectedCategoryChanged} />
  );

  constructor(props: {}) {
    super(props);
    this.state = { selectedCategory: null };
  }

  onSelectedCategoryChanged(name: string) {
    this.setState({ selectedCategory: name });
  }

  render() {
    return (
      <Provider inject={[categoryStore]}>
        <Row>
          <Col span={6}>{this.populatedCategoryTree}</Col>
          <Col span={18}>
            <PopulatedPartTable
              selectedCategory={this.state.selectedCategory}
            />
          </Col>
        </Row>
      </Provider>
    );
  }
}
