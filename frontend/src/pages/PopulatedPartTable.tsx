import React, {useMemo, useState} from "react";
import {IParameterType, IPart, ParameterTypeClient, PartClient} from "../models/api";
import {Skeleton, Table} from "antd";
import {SortOrder} from "antd/lib/table";
import {ApiClient, filterIn, ParameterValue, withLimit} from "../models/api_diy";

const partsClient = new PartClient();
const apiClient = new ApiClient();

function partsToParameters(parts: IPart[]) {
    return parts.flatMap(part =>
        part.parameters
            .map((param) => param.parameter_type))
        .sort()
}

function urlToId(url: string): number {
    return parseInt(
        url.split('/')
            .filter(value =>
                value.match(/^\d+$/))[0], 10)
}

function partsToTable(parts: IPart[]) {
    return parts.map(part => ({
        url: part.url,
        name: part.part_number,
        ...Object.assign({}, ...part.parameters.map(param => ({
            [param.parameter_type]: param.enum_value || (param.num_value || 0).toFixed(2)
        })))
    }))
}

export const PopulatedPartTable: React.FunctionComponent<{
    selectedCategory: string | null
}> = ({selectedCategory}) => {
    const [parts, setParts] = useState<IPart[] | null>(null);
    const [sort, setSort] = useState<[string, SortOrder]>(['id', 'descend'])
    const [parameters, setParameters] = useState<ParameterValue[] | null>(null)
    const [totalResults, setTotalResults] = useState<number>(0)
    const [currentPage, setCurrentPage] = useState<number>(0)
    const pageSize = 25

    useMemo(() => {
        partsClient.list(pageSize, currentPage * pageSize)
            .then(partsResult => {
                setTotalResults(partsResult.count)
                setParts(partsResult.results)
                const parameters = partsToParameters(partsResult.results)
                const parameterIds = parameters.map(urlToId)
                return apiClient.listParametersTypes([
                    withLimit(100),
                    filterIn('id', parameterIds)])
            })
            .then(parametersResult => {
                setParameters(parametersResult.results)
            })
    }, [selectedCategory, currentPage, setTotalResults])

    if (parts == null || parameters == null) {
        return <Skeleton/>
    }


    return <Table
        dataSource={partsToTable(parts)}
        pagination={{
            total: totalResults,
            current: currentPage + 1,
            pageSize: 25,
            onChange: (page) => setCurrentPage(page - 1),
        }}
        columns={[
            {
                title: "Name",
                dataIndex: "name",
                key: "name",
                sorter: true,
            },
            ...parameters.map(param => ({
                title: param.name,
                dataIndex: param.url,
                key: param.url,
                sorter: true,
            }))
        ]}
        rowSelection={{
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(
                    `selectedRowKeys: ${selectedRowKeys}`,
                    "selectedRows: ",
                    selectedRows
                );
            },
            getCheckboxProps: record => ({
                disabled: record.name === "Disabled User", // Column configuration not to be checked
                name: record.name
            })
        }}
    />
}