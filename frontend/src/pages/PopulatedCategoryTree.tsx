import React from "react";
import { Subscribe } from "unstated";
import { CategoriesContainer } from "../models/Categories";
import { CategoryTree } from "../components/CategoryTree";

interface PopulatedCategoryTreeFC {
  onCategorySelect(name: string): void;
}

export const PopulatedCategoryTree: React.FC<PopulatedCategoryTreeFC> = ({
  onCategorySelect
}) => (
  <Subscribe to={[CategoriesContainer]}>
    {categories => {
      const categoriesContainer = categories as CategoriesContainer;
      return (
        <CategoryTree
          onSelect={onCategorySelect}
          {...categoriesContainer.state}
        />
      );
    }}
  </Subscribe>
);
