import { Container } from "unstated";
import { Category, ICategory, CategoryClient } from "./api";

interface CategoriesState {
  categories: Category[];
}

const categoryClient = new CategoryClient();

export class CategoriesContainer extends Container<CategoriesState> {
  state: CategoriesState = {
    categories: []
  };

  async fetchAll() {
    await this.setState({ categories: await categoryClient.list() });
  }

  async createCategory(category: ICategory) {
    await categoryClient.create(new Category(category));
    // there's not an easy way for us to insert the new object in the
    // correct place in the category tree
    await this.fetchAll();
  }
}
