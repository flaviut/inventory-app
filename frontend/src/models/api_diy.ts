import {ParameterType} from "./api";

export class UrlParameter {
    constructor(public readonly name: string, public readonly value: string) {
    }
}


export enum ParameterTypeKind {
    Enum = "enum",
    Scalar = "scalar"
}

export interface EnumValue {
    id?: number | undefined;
    name: string;
    description?: string | undefined;
}

export type ParameterValue = {
    url: string;
    name: string;
    description: string;
} & ({
    kind: ParameterTypeKind.Enum;
    value_set: EnumValue[];
} | {
    kind: ParameterTypeKind.Scalar;
    unit: string;
    min_value: number;
    max_value: number;
})

export interface PaginatedListResult<T> {
    count: number;
    next: string | null;
    previous: string | null;
    results: T[];
}

export function withLimit(limit: number): UrlParameter {
    return new UrlParameter('limit', limit.toFixed(0))
}

export function withOffset(offset: number): UrlParameter {
    return new UrlParameter('offset', offset.toFixed(0))
}

export function filterIn(field: string, values: any[]): UrlParameter {
    return new UrlParameter(
        `${encodeURIComponent(field)}__in`,
        values.map(encodeURIComponent).join(','))
}


export class ApiClient {
    public readonly baseUrl = 'http://localhost:3000/api'

    private getUrl(path: string, args: UrlParameter[]) {
        return `${this.baseUrl}/${path}?${args.map(param => `${param.name}=${param.value}`).join('&')}`
    }

    listParametersTypes(args: UrlParameter[]): Promise<PaginatedListResult<ParameterValue>> {
        return fetch(this.getUrl('parameter-type', args))
            .then(response => response.json())
    }
}