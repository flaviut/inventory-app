import React, {
  useState,
  FC,
  useCallback,
  ChangeEventHandler,
  useMemo,
  ChangeEvent
} from "react";
import Search from "antd/lib/input/Search";
import { Category } from "../models/api";
import lodash, { Dictionary } from "lodash";

export interface CategoryFilterProps {
  setVisibleCategories(visibleIds: string[] | undefined): void;
  categoryIndex: Dictionary<Category>;
}

export function getCategoryIndex(categories: Category[]): Dictionary<Category> {
  return lodash.keyBy(categories, "url");
}

/** Gets the set of elements that are either in `matched` or parents of `matched`. */
function filterTree(
  index: Dictionary<Category>,
  matched: Category[]
): Category[] {
  const parents = matched
    .filter(cat => cat.parent != null)
    .map(cat => index[cat.parent!]);
  if (parents.length === 0) {
    return matched;
  }

  return [...matched, ...filterTree(index, parents)];
}

function normalizeText(text: string | undefined | null): string {
  if (text == null) {
    return "";
  }
  return text.toLowerCase().trim();
}

export const CategoryFilter: FC<CategoryFilterProps> = ({
  categoryIndex,
  setVisibleCategories
}) => {
  const [filterText, setFilterText] = useState("");
  const onChange: ChangeEventHandler<HTMLInputElement> = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => setFilterText(event.target.value),
    [setVisibleCategories, setFilterText]
  );

  const normalizedFilter = normalizeText(filterText);

  useMemo(() => {
    if (normalizedFilter.length === 0) {
      setVisibleCategories(undefined);
    }

    const filteredCategories = lodash.filter(
      categoryIndex,
      cat =>
        normalizeText(cat.name).includes(normalizedFilter) ||
        normalizeText(cat.description).includes(normalizedFilter)
    );
    const allCategories = filterTree(categoryIndex, filteredCategories).map(
      cat => cat.url!
    );
    setVisibleCategories(allCategories);
  }, [categoryIndex, normalizedFilter, setVisibleCategories]);

  return (
    <Search
      style={{ marginBottom: 8 }}
      placeholder="Search"
      onChange={onChange}
    />
  );
};
