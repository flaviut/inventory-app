import { Tree } from "antd";
import React, { FC, useState, useMemo, useCallback, useReducer } from "react";
import { Category } from "../models/api";
import { CategoryFilter, getCategoryIndex } from "./CategoryFilter";
import { AntTreeNodeExpandedEvent } from "antd/lib/tree";
import lodash from "lodash";

const { TreeNode } = Tree;

function getVisibleCategories(
  onSelect: (name: string) => void,
  categories: CategoryWithChild[],
  visibleCategories: string[] | undefined
) {
  return categories
    .filter(
      category =>
        visibleCategories == null ||
        visibleCategories.includes(category.category.url!)
    )
    .map(category => getCategoryElement(onSelect, category, visibleCategories));
}

// Don't actually use this as a component, or Ant design throws a fit.
// Just call it as a function.
const getCategoryElement = (
  onSelect: (name: string) => void,
  { category, children }: CategoryWithChild,
  visibleCategories: string[] | undefined
) => (
  <TreeNode
    key={category.url}
    title={category.name}
    onCheck={() => onSelect(category.name)}
  >
    {getVisibleCategories(onSelect, children, visibleCategories)}
  </TreeNode>
);

interface CategoryWithChild {
  category: Category;
  children: CategoryWithChild[];
}

function categoryListToCategoryWithChild(
  categories: Category[]
): CategoryWithChild[] {
  const keyToCat = lodash
    .chain(categories)
    .keyBy("url")
    .mapValues(
      category =>
        ({
          children: [],
          category
        } as CategoryWithChild)
    )
    .value();

  for (let category of categories) {
    if (category.parent == null) {
      continue;
    }
    keyToCat[category.parent].children.push(keyToCat[category.url!]);
  }

  return lodash
    .values(keyToCat)
    .filter(category => category.category.parent == null);
}

export interface CategoryTreeProps {
  onSelect(name: string): void;
  categories: Category[];
}

export const CategoryTree: React.FC<CategoryTreeProps> = ({
  onSelect,
  categories
}) => {
  const [visibleCategories, setVisibleCategories] = useState<
    string[] | undefined
  >(undefined);
  const categoryIndex = useMemo(() => getCategoryIndex(categories), [
    categories
  ]);
  const nestedCategories = useMemo(
    () => categoryListToCategoryWithChild(categories),
    [categories]
  );

  const [expandedKeys, setExpandedKeys] = useState<string[]>([]);
  const onExpand = useCallback(
    (_: string[], info: AntTreeNodeExpandedEvent) => {
      const nodeKey = info.node.props.key || info.node.props.eventKey;
      if (nodeKey == null) {
        throw new Error("Node does not contain key: " + info.node);
      }

      if (info.expanded) {
        setExpandedKeys([nodeKey, ...expandedKeys]);
      } else {
        setExpandedKeys(expandedKeys.filter(key => key != nodeKey));
      }
    },
    [setExpandedKeys, expandedKeys]
  );

  return (
    <React.Fragment>
      <CategoryFilter
        categoryIndex={categoryIndex}
        setVisibleCategories={setVisibleCategories}
      />
      <Tree
        expandedKeys={
          visibleCategories == null ? expandedKeys : visibleCategories
        }
        onExpand={onExpand}
      >
        {getVisibleCategories(onSelect, nestedCategories, visibleCategories)}
      </Tree>
    </React.Fragment>
  );
};
