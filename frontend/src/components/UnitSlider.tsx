import * as React from "react";
import { Unit } from "mathjs";
import { Slider } from "antd";
import mathjs from "mathjs";
import lodash from "lodash";

export interface UnitSliderProps {
  minValue: number;
  maxValue: number;
  isLog: boolean;

  unit: Unit;
}

interface UnitSliderState {
  value: number;
}

export class UnitSlider extends React.Component<
  UnitSliderProps,
  UnitSliderState
> {
  private numberToSlider(value: number): number {
    if (this.props.isLog) {
      return 2 ** value;
    } else {
      return value;
    }
  }

  private sliderToNumber(value: number): number {
    if (this.props.isLog) {
      return Math.log2(value);
    } else {
      return value;
    }
  }

  private generateLogMarks(): { [id: number]: string } {
    const maxDecade = Math.ceil(Math.log10(this.props.maxValue));
    const minDecade = Math.floor(Math.log10(this.props.maxValue));
    lodash.range(minDecade, maxDecade).map((v: number) => {
      const decadeStart = 10 ** v;
      const unit = mathjs
        .unit((decadeStart as unknown) as number, "m")
        .formatUnits();
      return [
        this.numberToSlider(decadeStart),
        unit.substring(0, unit.length - 1) // strip the "m", leaving just the prefix
      ];
    });
    return {};
  }

  public render(): React.ReactNode {
    return (
      <React.Fragment>
        <Slider
          range
          defaultValue={[
            this.numberToSlider(this.props.minValue),
            this.numberToSlider(this.props.maxValue)
          ]}
        />
      </React.Fragment>
    );
  }
}
